import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.swing.*;

public class ContactEntry extends JFrame {
    private JLabel cidLabel, nameLabel, emailLabel;
    private JTextField cidField, nameField, emailField;
    private JButton submitButton;

    public ContactEntry() {
        initComponents();
    }

    private void initComponents() {
        setLayout(new GridLayout(4, 3));

        cidLabel = new JLabel("Cid:");
        add(cidLabel);

        cidField = new JTextField();
        add(cidField);

        nameLabel = new JLabel("Name:");
        add(nameLabel);

        nameField = new JTextField();
        add(nameField);

        emailLabel = new JLabel("Email:");
        add(emailLabel);

        emailField = new JTextField();
        add(emailField);

        submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String contactID = cidField.getText();
                String name = nameField.getText();
                String email = emailField.getText();
                int cid = Integer.parseInt(contactID);

                insertIntoContacts(cid, name, email);
                JOptionPane.showMessageDialog(ContactEntry.this, "Contact saved successfully.");
            }
        });
        add(submitButton);

        pack();
        setTitle("Contact Entry");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private static void insertIntoContacts(int cid, String name, String email) {
        String sql = "INSERT INTO contacts(cid,name,email) VALUES(?,?,?)";
        int count = 0;
        int batchSize = 20;
        try (Connection connect = ContactSearch.connect();
                PreparedStatement stmt = connect.prepareStatement(sql)) {
            stmt.setInt(1, cid);
            stmt.setString(2, name);
            stmt.setString(3, email);

            stmt.addBatch();

            if (count % batchSize == 0) {
                stmt.executeBatch();
            }
            stmt.executeBatch();
            connect.commit();
            connect.close();
            System.out.println("Insertion Successful");
        } catch (SQLException e) {
            System.out.println("Insertion Error:\nRepetition of primary key: cid");
            System.out.println(e.getMessage());
        }
    }

    public static List<Contacts> readContacts(String fileName) throws SQLException {
        List<Contacts> contacts = new ArrayList<>();
        Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        try (
                BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            // read the first
            // line from the text
            // file
            String line = br.readLine();
            // loop until all lines are read
            while (line != null) {
                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
                String[] attributes = line.split(",");
                Contacts contact = createContacts(attributes);
                // adding contacts into ArrayList
                contacts.add(contact);
                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();

            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return contacts;
    }

    // returns parameters for Contacts class
    private static Contacts createContacts(String[] metadata) throws SQLException {
        int cid = Integer.parseInt(metadata[0]);
        String name = metadata[1];
        String email = metadata[2];

        insertIntoContacts(cid, name, email);

        return new Contacts(cid, name, email);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new ContactEntry().setVisible(true);
            }
        });
    }
}
