import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ContactSearch extends JFrame {
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel emailLabel;
    private JTextField emailTextField;
    private JButton searchButton;
    private Connection connection;

    public ContactSearch() {
        super("Contact Search");

        nameLabel = new JLabel("Name: ");
        nameTextField = new JTextField(20);
        emailLabel = new JLabel("Email: ");
        emailTextField = new JTextField(20);
        searchButton = new JButton("Search");

        setLayout(new FlowLayout());

        add(nameLabel);
        add(nameTextField);
        add(emailLabel);
        add(emailTextField);
        add(searchButton);

        connection = ContactSearch.connect();

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = nameTextField.getText();
                String email = emailTextField.getText();

                try (Statement statement = connection.createStatement();
                        ResultSet resultSet = statement.executeQuery(
                                "SELECT * FROM contacts WHERE name = '" + name + "' AND email = '" + email + "'")) {
                    while (resultSet.next()) {
                        System.out.println("Name: " + resultSet.getString("name"));
                        System.out.println("Email: " + resultSet.getString("email"));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });

        setSize(400, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    // Database Connection
    public static Connection connect() {
        Connection connect = null;
        String username = "root";
        String password = "";
        String url = "jdbc:sqlite:/C:\\Users\\Yuri\\sqlite\\usersdb.db";
        try {
            connect = DriverManager.getConnection(url, username, password);
            connect.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("Database connection unsuccessful.");
            System.out.println(e.getMessage());
        }
        return connect;
    }

    public static void main(String[] args) {
        new ContactSearch();
    }
}
