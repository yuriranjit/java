import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class Contacts {
  // private variables
  private int cid;
  private String name;
  private String email;

  // constructor
  public Contacts(int cid, String name, String email) {
    this.cid = cid;
    this.email = email;
    this.name = name;
  }

  // initialize email // setter
  public void setEmail(String email) {
    this.email = email;
  }

  // initialize name
  public void setName(String name) {
    this.name = name;
  }

  public void setCid(int cid) {
    this.cid = cid;
  }

  // access email // getter
  public String getEmail() {
    return this.email;
  }

  // access name
  public String getName() {
    return this.name;
  }

  public int getCid() {
    return this.cid;
  }

  // function to change email address
  // database.connect() is a function defined inside class database in
  // database.java file that connects to SQLITE database with jdbc
  public void changeEmailAddress(String newEmail) throws SQLException {
    String updateSql = "UPDATE contacts set email='" + newEmail + "' WHERE email='" + this.email + "'";
    try {
      Connection connect = database.connect();
      Statement statement = connect.createStatement();
      statement.executeUpdate(updateSql);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  // function to change name
  public void changeName(String newName) throws SQLException {
    String updateSql = "UPDATE contacts set name='" + newName + "' WHERE name='" + this.name + "'";
    try {
      Connection connect = database.connect();
      Statement statement = connect.createStatement();
      statement.executeUpdate(updateSql);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  // function to delete user id and details
  public void deleteUser(int cid) throws SQLException {
    String updateSql = "DELETE FROM contacts WHERE cid='" + this.name + "'";
    try {
      Connection connect = database.connect();
      Statement statement = connect.createStatement();
      statement.executeUpdate(updateSql);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return "Contacts [Contacts_ID=" + cid + ", name=" + name + ", email=" + email + "]";
  }
}
