import java.rmi.*;

public interface Factorial extends Remote {
    long calculateFactorial(int n) throws RemoteException;
}