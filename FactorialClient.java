import java.rmi.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FactorialClient {
    public static void main(String[] args) {
        try {
            int number;
            try (Scanner myObj = new Scanner(System.in)) {
                System.out.println("Enter number to calculate factorial: ");

                number = myObj.nextInt();
            }
            List<Factorial> servers = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                servers.add((Factorial) Naming.lookup("factorial"));
            }
            long result = 1;
            for (int i = 1; i <= number; i++) {
                Factorial server = servers.get((i - 1) % 3);
                result *= server.calculateFactorial(i);
            }
            System.out.println("The factorial of " + number + " is: " + result);
        } catch (Exception e) {
            System.out.println("Factorial Client Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}