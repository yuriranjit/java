import java.rmi.*;
import java.rmi.server.*;

public class FactorialServer extends UnicastRemoteObject implements Factorial {
    public FactorialServer() throws RemoteException {
        super();
    }

    public long calculateFactorial(int n) throws RemoteException {
        if (n == 0) {
            return 1;
        }
        return n * calculateFactorial(n - 1);
    }

    public static void main(String[] args) {
        try {
            FactorialServer server = new FactorialServer();
            Naming.rebind("factorial", server);
            System.out.println("Factorial Server Ready");
        } catch (Exception e) {
            System.out.println("Factorial Server Main Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}