import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;

public class LoginPage extends JFrame {

  private JTextField txtUsername;
  private JPasswordField txtPassword;
  private JButton lButton;
  private JPanel contentPane;

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          LoginPage frame = new LoginPage();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  // Database Connection
  public static Connection connect() {
    Connection connect = null;
    String username = "root";
    String password = "";
    String url = "jdbc:sqlite:/C:\\Users\\ROHAN\\Downloads\\sqlite-tools-win32-x86-3400000\\usersdb.db";
    try {
      connect = DriverManager.getConnection(url, username, password);
      connect.setAutoCommit(false);
    } catch (SQLException e) {
      System.out.println("Database connection unsuccessful.");
      System.out.println(e.getMessage());
    }
    return connect;
  }

  private boolean checkLogin(String username, String password) {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet result = null;

    try {
      // establish a database connection
      connection = LoginPage.connect();

      // prepare SQL statement to check the user credentials against the database
      statement = connection.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?");
      statement.setString(1, username);
      statement.setString(2, password);

      // execute the statement and get the result set
      result = statement.executeQuery();

      // return true if a record is found, false otherwise
      return result.next();
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    } finally {
      try {
        if (result != null) {
          result.close();
        }
        if (statement != null) {
          statement.close();
        }
        if (connection != null) {
          connection.close();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public LoginPage() {
    initComponents();
  }

  private void initComponents() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);

    JLabel lblUsername = new JLabel("Username");
    lblUsername.setBounds(50, 70, 100, 14);
    contentPane.add(lblUsername);

    JLabel lblPassword = new JLabel("Password");
    lblPassword.setBounds(50, 121, 100, 14);
    contentPane.add(lblPassword);

    txtUsername = new JTextField();
    txtUsername.setBounds(150, 67, 200, 20);
    contentPane.add(txtUsername);
    txtUsername.setColumns(10);

    txtPassword = new JPasswordField();
    txtPassword.setBounds(150, 118, 200, 20);
    contentPane.add(txtPassword);

    lButton = new JButton("Login");
    lButton.setBounds(150, 169, 200, 20);
    lButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String username = txtUsername.getText();
        String password = new String(txtPassword.getPassword());

        if (checkLogin(username, password)) {
          JOptionPane.showMessageDialog(LoginPage.this, "Welcome, " + username);
        } else {
          JOptionPane.showMessageDialog(LoginPage.this, "Invalid Login Credentials",
              "Login Error",
              JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    contentPane.add(lButton);

    pack();
    setTitle("Login Page");
  }

}
