import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class database {
    public static void main(String[] args)
            throws FileNotFoundException, ParseException, SQLException, ClassNotFoundException {
        // load data from csv
        List<Contacts> contacts = readContactsFromCSV("contacts.csv");
        for (Contacts c : contacts) {
            System.out.println(c);
        }

        database app = new database();
        System.out.println("\nReading Data From Database table:");
        app.readDataFromTable();

    }

    // Database Connection
    public static Connection connect() {
        Connection connect = null;
        String username = "root";
        String password = "";
        String url = "jdbc:sqlite:/C:\\Users\\ROHAN\\Downloads\\sqlite-tools-win32-x86-3400000\\usersdb.db";
        try {
            connect = DriverManager.getConnection(url, username, password);
            connect.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("Database connection unsuccessful.");
            System.out.println(e.getMessage());
        }
        return connect;
    }

    // Read Data from Sqlite table using java
    public void readDataFromTable() throws SQLException {
        String selectSql = "SELECT * FROM contacts";
        try (
                Connection connect = database.connect();
                Statement statement = connect.createStatement();) {
            ResultSet result = statement.executeQuery(selectSql);
            while (result.next()) {
                int cid = result.getInt("cid");
                String name = result.getString("name");
                String email = result.getString("email");

                System.out.println("Contacts:{" + cid + "|" + name + "|" + email + "}");
            }
        }
    }

    /**
     * 
     * @throws SQLException
     */

    public static List<Contacts> readContactsFromCSV(String fileName) throws SQLException {
        List<Contacts> contacts = new ArrayList<>();
        Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        try (
                BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
            // read the first
            // line from the text
            // file
            String line = br.readLine();
            // loop until all lines are read
            while (line != null) {
                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
                String[] attributes = line.split(",");
                Contacts contact = createContacts(attributes);
                // adding contacts into ArrayList
                contacts.add(contact);
                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();

            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return contacts;
    }

    // Inserting csv data into database Table contacts
    private static void insertIntoContacts(int cid, String name, String email) {
        String sql = "INSERT INTO contacts(cid,name,email) VALUES(?,?,?)";
        int count = 0;
        int batchSize = 20;
        try (Connection connect = database.connect();
                PreparedStatement stmt = connect.prepareStatement(sql)) {
            stmt.setInt(1, cid);
            stmt.setString(2, name);
            stmt.setString(3, email);

            stmt.addBatch();

            if (count % batchSize == 0) {
                stmt.executeBatch();
            }
            stmt.executeBatch();
            connect.commit();
            connect.close();
            System.out.println("Insertion Successful");
        } catch (SQLException e) {
            System.out.println("Insertion Error:\nRepetition of primary key: cid");
            System.out.println(e.getMessage());
        }
    }

    // returns parameters for Contacts class
    private static Contacts createContacts(String[] metadata) throws SQLException {
        int cid = Integer.parseInt(metadata[0]);
        String name = metadata[1];
        String email = metadata[2];

        insertIntoContacts(cid, name, email);

        return new Contacts(cid, name, email);
    }

}