// Java program to send email

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class sendemail {
    database db;

    public static void main(String[] args) throws MessagingException {
        // email ID of receipent
        String recipient = "ram@gmail.com,shyam@gmail.com,sita@gmail.com,gita@gmail.com";
        // email ID of Sender.
        String sender = "1911021_yuri@kusom.edu.np";
        String password = "password";

        // Getting system properties
        Properties properties = System.getProperties();

        // Setting up mail server
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "143");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.trust", "*");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.starttls.required", "true");
        properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        // creating session object to get properties
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender, password);
            }
        });

        Message message = prepareMessage(session, sender, recipient);
        // Transport transport = session.getTransport("smtp");
        // send message to receipent
        Transport.send(message);
        System.out.println("Mail successfully sent");
    }

    private static Message prepareMessage(Session session, String sender, String recipient) {
        try {
            InternetAddress[] iAdressArray = InternetAddress.parse(recipient);
            // MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From Field: adding senders email to from field.
            message.setFrom(new InternetAddress(sender));

            // Set To Field: adding recipient's email to from field.
            message.setRecipients(Message.RecipientType.TO, iAdressArray);

            // Set Subject: subject of the email
            message.setSubject("Marketing.java");

            // set body of the email.
            message.setText(
                    "Dear Ram Hari,\n We are going to launch the new Outlet @ New Road." +
                            "Please visit in the opening date and grab the heavy discounts upto 80%.\n\n Thank you,\nABC Outlet");

            return message;
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
        return null;
    }
}
